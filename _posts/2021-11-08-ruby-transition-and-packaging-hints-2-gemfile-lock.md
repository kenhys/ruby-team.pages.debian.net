---
layout: post
author: Daniel Leidert
title: 'Ruby transition and packaging hints #2 - Gemfile.lock created by bundler/setup with Ruby 2.7 preventing successful test with Ruby 3.0'
date: '2021-11-08 23:31 +0100'
description: >-
 Packages using 'bundler/setup' create a Gemfile.lock file. However, this file
 contains references not available for Ruby 3 (e.g. racc-1.4.16) and therefor
 bundler fails with Bundler::GemNotFound. The fix is to remove the lock files
 during the test runs.
tags:
- ftbfs
- bundler
- gemfile.lock
- ruby3.0
- packaging
---

We currently face an issue in all packages requiring `bunlder/setup` and trying
to run the tests for Ruby 2.7 and 3.0. The problem is that the first tests will
create `Gemfile.lock` (or `gemfile/gemfile-*.lock`) using Ruby 2.7 and the next
run for Ruby 3 will report e.g.:

```
Failure/Error: require 'bundler/setup' # Set up gems listed in the Gemfile.

Bundler::GemNotFound:
  Could not find racc-1.4.16 in any of the sources
```

or

```
/usr/share/rubygems-integration/all/gems/bundler-2.2.27/lib/bundler/definition.rb:496:in `materialize':
  Could not find rexml-3.2.3.1 in any of the sources (Bundler::GemNotFound)
```

Both bugs [#996207](https://bugs.debian.org/996207) and
[#996302](https://bugs.debian.org/996302) are incarnations of this issue. The
fix is as easy as making sure that the `.lock` files are removed before each
run. This can be done in e.g. `debian/ruby-tests.rake` as very first task:

```
File.delete("Gemfile.lock") if File.exist?("Gemfile.lock")
```

In [another](https://bugs.debian.org/996299) case the `.lock` file is created
by the tests in `gemfiles/`. While the first examples could actually be solved
by `gem2deb` removing `Gemfile.lock` on its own, I'm not quite sure how to
handle the last case using packaging tools.

The interesting part is that we will unlikely be confronted with this issue
anytime soon again. It seems very specific to the Ruby 3.0 transition.

## Update

After talking to Antonio he added some code to `gem2deb-test-runner` to moving
`Gemfile.lock` files out of the way. The tool already did this in an
`autopkgtest` environment. In the upcoming 1.7 release it will do it in general
and this will fix some more FTBFSes, e.g. #998497 and #996141 - originally
reported against `ruby-voight-kampff` and `ruby-bootsnap`.

<!-- # vim: set tw=79 ts=2 sw=2 ai si et: -->
